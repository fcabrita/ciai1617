package ciai;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class AppController {

	@RequestMapping(value = "")
	public String root() {
		return "index";
	}
}
